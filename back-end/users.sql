-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : dim. 16 juin 2024 à 21:53
-- Version du serveur : 8.0.37-0ubuntu0.22.04.3
-- Version de PHP : 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `events`
--

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `firstname`, `email`, `password`) VALUES
(5, 'richard', 'Combe', 'richrd.combe@gmail.com', '$2y$10$Gw.8.2QxWAUN3tXtIZTxS..Y2xKs/0gdYeBFrtKAULwhLaAMikbdu'),
(6, 'vimardd', 'cyril', 'cyril.vimard', '$2y$10$DaErVTwobCOSrYB5MWCC0ejxhfbuMwKWmcjY8/nw5GOCswyv1Kxea'),
(7, 'grange', 'matéo', 'grange.moudzouma@gmail.com', '$2y$10$ZdednBDhQh2vgkRwivqOpu0c6BWrg/dQJ5ofV2QfolKTZS4pOcE3q'),
(8, 'guillaume', 'simon', 'guillaume.simon@gmail.com', '$2y$10$ttrNgMAzlhv1dna7Ay8YaOClZlvizO4vKx7zFBkFs5DPk8IeADG7y'),
(9, 'Pablo', 'simon', 'guillaume.simon@gmail.com', '$2y$10$Nrx2XhVkU13MIh3YH/9.NeRtBRyLKwM5iVXEtfnqH5kkc22EKGcu2'),
(10, 'LOULEMBO', 'Marc', 'marc.loulembo@gmail.com', '$2y$10$yA/6ro2djsdG.t7u3Y/0QOYLT7.FwOWB4RUdtRBUo1k5cxyb19oA.'),
(12, 'BADELAS', 'Isaiah ', 'isaiah.badelas@gmail.com', '$2y$10$9QyB.pdzWdTUPn4zIBEP6.dkjm7N8kpT22NAa9cvaevu3wCjXyXNS');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
